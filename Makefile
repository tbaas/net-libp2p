VERSION221=v2.2.1
VERSION222=v2.2.2
gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION221)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION222)
	go get chainmaker.org/chainmaker/libp2p-pubsub@v1.1.2
	go get chainmaker.org/chainmaker/net-common@v1.1.1
	go get chainmaker.org/chainmaker/logger/v2@$(VERSION221)
	go mod tidy
